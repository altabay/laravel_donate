<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class Company extends Model
{

    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'amount_need', 'amount_now', 'type', 'status', 'user_id', 'description', 'images',
        'category_id', 'category_id', 'visitor_comments', 'donations_enabled', 'auto_fb_post_mode', 'visible',
        'zip_postal'
    ];

    /**
     * create company
     * @param $request
     */
    public function createCompany($request)
    {
        $imagesArray = $this->imagesSave($request);

        list($visitorComments, $donationsEnabled, $autoFb, $visible) = $this->companySettings($request);

        $company = Company::create([
            'name' => $request->name,
            'amount_need' => $request->amount_need,
            'amount_now' => "0",
            'type' => 'simple',
            'status' => 'moderate',
            'description' => $request->description,
            'user_id' => Auth::user()->id,
            'images' => \GuzzleHttp\json_encode($imagesArray),
            'visitor_comments' => $visitorComments,
            'donations_enabled' => $donationsEnabled,
            'auto_fb_post_mode' => $autoFb,
            'visible' => $visible,
            'zip_postal' => $request->zip_postal,
            'category_id' => $request->category_id
        ]);

        return $company;
    }

    /**
     * edit company
     * @param $request
     */
    public function editCompany($request){
        $company = Company::where('id', '=', $request->id)->where('user_id', '=', Auth::user()->id)->first();
        list($visitorComments, $donationsEnabled, $autoFb, $visible) = $this->companySettings($request);
        $imagesArray = $this->imagesSave($request, $company, "edit");
        $company->name = $request->name;
        $company->amount_need = $request->amount_need;
        $company->description = $request->description;
        $company->user_id = Auth::user()->id;
        $company->images = \GuzzleHttp\json_encode($imagesArray);
        $company->visitor_comments = $visitorComments;
        $company->donations_enabled = $donationsEnabled;
        $company->auto_fb_post_mode = $autoFb;
        $company->visible = $visible;
        $company->zip_postal = $request->zip_postal;
        $company->category_id = $request->category_id;
        $company->save();
    }

    /**
     * Save images
     * @param $request
     * @param $input
     * @return array
     */
    public function imagesSave($request, $company = null, $flug = null)
    {
        $imagesArray = [];
        $images = $request->file('images');
        if(isset($flug) && !isset($images)){
            $imagesArray = \GuzzleHttp\json_decode($company->images);
            return $imagesArray;
        }
        if(isset($images)) {
            foreach ($images as $image) {
                $input['imagename'] = rand(100, 10000).time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('thumbnail');
                $img = Image::make($image->getRealPath());
                $img->save($destinationPath . '/' . $input['imagename']);
                $imagesArray[] = $input['imagename'];
            }
        }
        return $imagesArray;
    }

    /**
     * @param $request
     * @return array
     */
    public function companySettings($request)
    {
        if (isset($request->visitor_comments)) {
            $visitorComments = 1;
        } else {
            $visitorComments = 0;
        }
        if (isset($request->donations_enabled)) {
            $donationsEnabled = 1;
        } else {
            $donationsEnabled = 0;
        }
        if (isset($request->auto_fb_post_mode)) {
            $autoFb = 1;
        } else {
            $autoFb = 0;
        }
        if (isset($request->visible)) {
            $visible = 1;
            return array($visitorComments, $donationsEnabled, $autoFb, $visible);
        } else {
            $visible = 0;
            return array($visitorComments, $donationsEnabled, $autoFb, $visible);
        }
    }


}
