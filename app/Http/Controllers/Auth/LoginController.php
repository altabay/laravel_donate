<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Laravel\Socialite\Facades\Socialite;
use App\SocialAccountService;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Social
     **/
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->fields([
            'first_name', 'last_name', 'email', 'gender', 'birthday'
        ])->scopes([
            'email', 'user_birthday'
        ])->redirect();
    }

    public function handleProviderCallback(SocialAccountService $service)
    {
        //$user = Socialite::driver('facebook')->fields(['first_name', 'last_name', 'email', 'gender', 'name'])->user();
        //$email =  $user->getEmail();
        //$userFind = User::where("email", '=', $email)->first();
        $userFind = $service->createOrGetUser(Socialite::driver('facebook')->fields(['first_name', 'last_name', 'email', 'gender', 'name'])->user(), "facebook");
        if(isset($userFind->id)){
            auth()->login($userFind);
            return redirect()->to('/home');
        } else {
            echo "user_not_found";
            exit;
        }
    }

   /* public function redirectToProviderTwitter()
    {
        return Socialite::driver('twitter')->redirect();
    }

    public function handleProviderCallbackTwitter(SocialAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('twitter')->user());
        auth()->login($user);
        return redirect()->to('/home');
    }*/

    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallbackGoogle(SocialAccountService $service){
        $userFind = $service->createOrGetUser(Socialite::driver('google')->user(), "google");
        if(isset($userFind->id)){
            auth()->login($userFind);
            return redirect()->to('/home');
        } else {
            echo "user_not_found";
            exit;
        }
    }


}
