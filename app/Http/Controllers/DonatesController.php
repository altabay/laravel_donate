<?php

namespace App\Http\Controllers;

use App\Company;
use App\Donate;
use App\Http\Requests\DonateFirstStep;
use App\StripeCheckout;
use App\StripeCustomer;
use App\WepayCheckout;
use App\WepayCreditCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class DonatesController
 * @package App\Http\Controllers
 */
class DonatesController extends Controller
{

    private $companyId = "";
    private $wepayCheckoutId = "";
    private $amount = "";

    public function index()
    {
        $userId = Auth::user()->id;
        $companies = Company::where("user_id", "=", $userId)->get();
        return view('donate', ['companies' => $companies]);
    }

    /**
     * first donate step
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function donateFor($slug)
    {
        $company = Company::where("slug", "=", $slug)->first();
        return view('donate.donate1', ['company' => $company]);
    }

    public function donateStep2(DonateFirstStep $request)
    {
        //Нужно брать ту платежку к которой привязана компания...
        $company = DB::select("SELECT u.pay_account_type FROM companies AS c JOIN users as u WHERE c.id = $request->company_id AND u.id = c.user_id");
        if ($company[0]->pay_account_type == "wepay") {
            if(Auth::check()){
                $creditCards = WepayCreditCard::where('user_id', '=', Auth::user()->id)->get();
            } else {
                $creditCards = null;
            }
            return view('donate.donate2wepay', ['request' => $request, 'creditCards' => $creditCards]);
        } else {
            if(Auth::check()){
                $creditCardsCustomer = StripeCustomer::where('user_id', '=', Auth::user()->id)->get();
            } else {
                $creditCardsCustomer = null;
            }
            return view('donate.donate2stripe', ['request' => $request, 'creditCardsCustomer' => $creditCardsCustomer]);
        }
    }

    public function stripePay(Request $request)
    {
        $card = $request->card;
        $expMonth = (int)$request->expityMonth;
        $expYear = (int)$request->expityYear;
        $cvc = $request->ccv;
        $amount = $request->amount;
        $companyId = $request->company_id;
        $email = $request->email;
        $anonymous = $request->anonymous;
        if(!isset($anonymous)) {
            $anonymous = 0;
        }

        if (isset($request->save_credit_card) && $request->save_credit_card == "true"){
            $saveCustomer = 1;
        } else {
            $saveCustomer = 0;
        }

        //selected_customer
        if(isset($request->selected_customer)){
            $selectedCustomerId = $request->selected_customer;
        } else {
            $selectedCustomerId = null;
        }

        $res = DB::select("SELECT sa.account_id FROM companies AS c JOIN users AS u JOIN stripe_account_register AS sa WHERE c.user_id = u.id AND sa.user_local_id = u.id");
        $accountId = $res[0]->account_id;

        $stripe = new StripeController();
        $result = $stripe->creatingDestinationCharges($card, $expMonth, $expYear, $cvc, $amount, $accountId, $email, $saveCustomer, $selectedCustomerId);
        if (isset($result->status) && $result->status == "succeeded") {
            if (Auth::check()) {
                $userId = Auth::user()->id;
            } else {
                $userId = "";
            }

            $stripeCheckount = new StripeCheckout();
            $stripeCheckount->stripeCheckoutSave($result, $userId, $companyId);

            $donate = new Donate();
            $donate->createDotate($result, $companyId, $userId, $anonymous, 1);

            $this->stripeBalanceRecalculation($companyId, $amount);
            return response()->json($result);

        } elseif (isset($result->error)) {
            return response()->json($result);
        } else {
            echo "some error";
            exit;
        }
    }

    /**
     * recalculation company balance after stripe pay
     * @param $companyId
     * @param $amount
     */
    public function stripeBalanceRecalculation($companyId, $amount)
    {
        $company = Company::find($companyId);
        $company->amount_now = $company->amount_now + $amount;
        $company->save();
    }

    /**
     * Recalculation all balance. Wepay
     */
    public function balanceReCalculationAll()
    {
        $this->checkoutUpdateStatusAll();
        $checkouts = WepayCheckout::where('state', '=', "released")->get();
        foreach ($checkouts as $checkout) {
            if ($checkout->state == "released" && $checkout->balance_included == 0) {
                $company = Company::find($checkout->reference_id);
                $this->companyId = $company->id;
                $this->wepayCheckoutId = $checkout->id;
                $this->amount = $company->amount_now + ($checkout->amount - $checkout->amount / 100 * 5); //minus 5%
                DB::transaction(function () {
                    DB::table('companies')->where('id', $this->companyId)->update(['amount_now' => $this->amount]);
                    DB::table('wepay_checkouts')->where('id', $this->wepayCheckoutId)->update(['balance_included' => 1]);
                });
            }
        }
        echo "done";
    }

    /**
     * Update checkouts state, that not "released". Wepay
     * wepay
     */
    public function checkoutUpdateStatusAll()
    {
        $checkouts = WepayCheckout::where('state', '!=', "released")->get();
        foreach ($checkouts as $checkout) {
            $res = DB::select("SELECT wu.access_token FROM wepay_accounts_register AS wa JOIN wepay_users_register as wu where wa.account_id = $checkout->account_id and wa.owner_user_id = wu.user_id");
            $token = $res[0]->access_token;
            $wepayController = new WepayController();
            $checkoutInfo = $wepayController->checkoutInfo($checkout->checkout_id, $token);
            if ($checkout->state != $checkoutInfo->state) {
                $checkout->state = $checkoutInfo->state;
                $checkout->save();
            }
        }
    }

}
