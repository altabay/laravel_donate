<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class OptionsController extends Controller
{

    public function success()
    {
        return view('pay_succeeded');
    }

    public function setCountry(Request $request) {
        if(!isset($_COOKIE["country"])){
            setcookie("country", $request->country, time() + (86400 * 30), "/");
        }
        return response()->json(['status' => 'complete']);
    }
}
