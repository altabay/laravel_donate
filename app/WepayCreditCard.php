<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WepayCreditCard extends Model
{

    protected $table = "wepay_credit_cards";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'cc_number',
        'user_id',
        'credit_card_id',
        'state',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
