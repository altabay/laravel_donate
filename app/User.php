<?php

namespace App;

use App\Http\Controllers\StripeController;
use App\Http\Controllers\WepayController;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'password',
        'system_status',
        'last_ip',
        'country',
        'status',
        'last_ip',
        'timezone',
        'ref',
        'is_admin_user',
        'pay_account_type',
        'newsletters',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    //$managed, $email, $country
    public static function boot()
    {
        self::creating(function ($model) {
            if ($model->country == "US") {
                $model->pay_account_type = "wepay";
                $wepay = new WepayController();
                $wepay->customAccountCreation($model);
            } else {
                $model->pay_account_type = "stripe";
                $spripe = new StripeController();
                $spripe->createAnAccount("false", $model->email, $model->country, $model->id);
            }
        });

        self::created(function ($model) {
            //set user_local_id to stripe_account_register after save user
            if($model->pay_account_type == "stripe") {
                $account = StripeAccountRegister::where('email', '=', $model->email)->first();
                $account->user_local_id = $model->id;
                $account->save();
            } else {
                $user = WepayUserRegister::where('email', '=', $model->email)->first();
                $user->user_local_id = $model->id;
                $user->save();
            }
        });
    }


}
