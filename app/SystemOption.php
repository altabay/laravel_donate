<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemOption extends Model
{

    protected $table = "system_options";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value'
    ];

}
