@extends('layouts.app')

<style>
    #app .container {
        width: 100%;
    }
</style>
<link href="{{asset('css/social-buttons.css')}}" rel="stylesheet">

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h3>Donate for company</h3>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-4">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">
                                                        Payment Details
                                                    </h3>
                                                </div>
                                                <div class="panel-body">
                                                    @if($errors->any())
                                                        <h4>{{$errors->first()}}</h4>
                                                    @endif

                                                        @if(isset($creditCardsCustomer) && !empty($creditCardsCustomer))
                                                            <p>Select card</p>
                                                            @foreach($creditCardsCustomer as $card)
                                                                <label>{{$card->card}}</label>
                                                                <input type="radio" name="selected_customer" value="{{$card->customer_id}}">
                                                                <br>
                                                            @endforeach
                                                            <hr>
                                                        @endif

                                                    <p class="error_place" style="color: red"></p>
                                                        <label>New Card</label>
                                                        <input type="radio" checked name="selected_customer" value="new_card">
                                                    <form id="payform" role="form" method="post" action="/stripe/pay">
                                                        <input type="hidden" id="amount" name="amount" value="{{$request->amount}}">
                                                        <input type="hidden" id="company_id" required name="company_id" value="{{$request->company_id}}">
                                                        <input type="hidden" id="email" required name="email" value="{{$request->email}}">
                                                        <input type="hidden" id="comment" required name="comment" value="{{$request->comment}}">
                                                        <input type="hidden" id="first_name" required name="first_name" value="{{$request->first_name}}">
                                                        <input type="hidden" id="last_name" required name="last_name" value="{{$request->last_name}}">
                                                        <input type="hidden" id="donationAnonymous" required name="donationAnonymous" value="{{$request->donationAnonymous}}">

                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <label for="cardNumber">
                                                                CARD NUMBER</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="cardNumber" name="card" placeholder="Valid Card Number"
                                                                       required autofocus />
                                                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-7 col-md-7">
                                                                <div class="form-group">
                                                                    <label for="expityMonth">
                                                                        EXPIRY DATE</label>
                                                                    <div class="col-xs-6 col-lg-6 pl-ziro">
                                                                        <input type="text" class="form-control" name="expityMonth" id="expityMonth" placeholder="MM" required />
                                                                    </div>
                                                                    <div class="col-xs-6 col-lg-6 pl-ziro">
                                                                        <input type="text" class="form-control" name="expityYear" id="expityYear" placeholder="YY" required /></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-5 col-md-5 pull-right">
                                                                <div class="form-group">
                                                                    <label for="cvCode">
                                                                        CV CODE</label>
                                                                    <input type="password" class="form-control" name="ccv" id="cvCode" placeholder="CV" required />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="checkbox pull-right" @if(!\Illuminate\Support\Facades\Auth::check())style="display: none"@endif>
                                                            <label>
                                                                <input id="save_credit_card" type="checkbox" />
                                                                Remember credit card
                                                            </label>
                                                        </div>

                                                        <br/>
                                                        <button id="submit_pay" type="submit" href="" class="btn btn-success btn-lg btn-block" role="button">Pay</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("js")

    <script>


        $("#submit_pay").on("click", function () {
            value = $("input[name='selected_customer']:checked").val();
            if(value != "new_card"){
                event.preventDefault();
                $("#payform").submit();
            }
        });


        $( "#payform" ).submit(function( event ) {
            event.preventDefault();
            amount = $('#amount').val();
            company_id = $('#company_id').val();
            cvCode = $('#cvCode').val();
            expityMonth = $('#expityMonth').val();
            expityYear = $('#expityYear').val();
            cardNumber = $('#cardNumber').val();
            saveCreditCard = $('#save_credit_card').prop('checked');
            email = $('#email').val();

            comment = $('#comment').val();
            donationAnonymous = $('#donationAnonymous').val();
            userName = $('#first_name').val() + " " + $('#last_name').val();

            callApiStripeCharge();
        });


        function callApiStripeCharge() {

            value = $("input[name='selected_customer']:checked").val();

            if(value == "new_card") {

                objectPay = {
                    _token: window.Laravel.csrfToken,
                    amount: amount,
                    company_id: company_id,
                    ccv: cvCode,
                    expityMonth: expityMonth,
                    expityYear: expityYear,
                    card: cardNumber,
                    email: email,
                    save_credit_card: saveCreditCard,
                    anonymous: donationAnonymous,
                    comment: comment,
                    first_name: userName
                }

                $.ajax({
                    url: "/stripe/pay",
                    type: "post",
                    dataType: 'json',
                    data:  objectPay ,
                    success: function (data) {
                        if(typeof data.error != "undefined") {
                            $(".error_place").text(data.error.message)
                        } else {
                            window.location.href = "/success/page?type=stripe";
                        }
                    }
                });

            } else {

                objectPay = {
                    _token: window.Laravel.csrfToken,
                    amount: amount,
                    company_id: company_id,
                    ccv: cvCode,
                    expityMonth: expityMonth,
                    expityYear: expityYear,
                    card: cardNumber,
                    email: email,
                    selected_customer: value,
                    anonymous: donationAnonymous,
                    comment: comment,
                    first_name: userName
                }

                $.ajax({
                    url: "/stripe/pay",
                    type: "post",
                    dataType: 'json',
                    data:  objectPay ,
                    success: function (data) {
                        if(typeof data.error != "undefined") {
                            $(".error_place").text(data.error.message)
                        } else {
                            window.location.href = "/success/page?type=stripe";
                        }
                    }
                });

            }

        }


    </script>

@endsection


