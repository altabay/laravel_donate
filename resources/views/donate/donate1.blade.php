@extends('layouts.app')

<style>
    #app .container {
        width: 100%;
    }
</style>
<link href="{{asset('css/social-buttons.css')}}" rel="stylesheet">

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <h3>Donate for company</h3>

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">

                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <form method="post" action="/donate/step2">
                                        {!! csrf_field() !!}
                                        <div class="input-group-addon">$</div>
                                        <input type="text" required class="form-control" id="exampleInputAmount" placeholder="Amount" name="amount">
                                        <input type="hidden" required name="company_id" value="{{$company->id}}">
                                        <div class="input-group-addon">.00</div>
                                        <input type="text" required class="form-control" id="exampleInputAmount" @if(\Illuminate\Support\Facades\Auth::check()) value="{{\Illuminate\Support\Facades\Auth::user()->name}}" @endif placeholder="First Name" name="first_name">
                                        <input type="text" required class="form-control" id="exampleInputAmount" @if(\Illuminate\Support\Facades\Auth::check()) value="{{\Illuminate\Support\Facades\Auth::user()->last_name}}" @endif placeholder="Last Name" name="last_name">
                                        <input type="text" required class="form-control" id="exampleInputAmount" placeholder="Email" name="email">
                                        <input type="text" required class="form-control" id="exampleInputAmount" placeholder="Comment" name="comment">
                                        <input type="text" required class="form-control" id="exampleInputAmount" placeholder="Zip" name="zip">
                                        @include('layouts.country_select')
                                        <input class="css-checkbox" id="anonymous" name="donationAnonymous" type="checkbox" value="1">
                                        <label class="css-label" for="anonymous">Hide name and comment from everyone but the organizer.</label>
                                        <p></p>
                                        <button class="btn btn-primary" style="display: block; margin: 0 auto">Continue</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")

@endsection




