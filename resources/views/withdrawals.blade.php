@extends('layouts.app')

<style>
    #app .container {
        width: 100%;
    }
</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <h3>User withdrawals settings</h3>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="withdrawal_div"></div>
                                    <script type="text/javascript" src="https://www.wepay.com/min/js/iframe.wepay.js"></script>
                                    <script type="text/javascript">
                                        WePay.iframe_checkout("withdrawal_div", "https://stage.wepay.com/api/account_update/{{$response->account_id}}?iframe=1&redirect_uri=http://donor.loc/home");
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
@endsection


{{--<div id="withdrawal_div"></div>
<script type="text/javascript" src="https://www.wepay.com/min/js/iframe.wepay.js"></script>
<script type="text/javascript">
    WePay.iframe_checkout("withdrawal_div", "https://stage.wepay.com/api/account_update/{{$response->account_id}}?iframe=1&redirect_uri=http%3A%2F%2Fexample.com%2Fwithdraw%2Fsuccess%2F");
</script>--}}



