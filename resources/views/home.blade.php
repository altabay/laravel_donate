@extends('layouts.app')

<style>
    #app .container {
        width: 100%;
    }
</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if(\Illuminate\Support\Facades\Auth::user()->pay_account_type == "wepay")
                        <p><a target="_blank" href="/withdrawals">My pay Account</a></p>
                        @else
                        <p><a target="_blank" href="https://dashboard.stripe.com/dashboard">My pay Account</a></p>
                        @endif


                        <h3>My Companies</h3>
                        <a class="btn btn-primary" href="/add/new/company">Add New Company</a>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="bs-example" data-example-id="simple-table">
                                        <table class="table">
                                            <caption>Optional table caption.</caption>
                                            <thead>
                                            <tr>
                                                <th>name</th>
                                                <th>amount_need</th>
                                                <th>amount_now</th>
                                                <th>action</th>
                                                <th>view</th>
                                                <th>edit</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($companies as $company)
                                                <tr class="company-{{$company->id}}">
                                                    <th>{{$company->name}}</th>
                                                    <th>{{$company->amount_need}}</th>
                                                    <th>{{$company->amount_now}}</th>
                                                    <th><span onclick="removeItemAjax('{{$company->id}}')" class="glyphicon glyphicon-remove-sign"
                                                              style="cursor: pointer"></span></th>
                                                    <th><a href="/company/{{$company->slug}}">View</a></th>
                                                    <th><a href="/edit/company/{{$company->slug}}">Edit</a></th>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("js")
  {{--  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>--}}
    <script>
        removeCompanyUrl = "/remove/company";
        classElement = "company-";
    </script>
    @include('layouts.confirm_remove')

@endsection

<div id="fb-root"></div>

