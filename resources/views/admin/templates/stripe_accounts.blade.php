@extends('admin.index')

@section("css")
@endsection

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Users Table <small>Users configurations</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <button class="btn btn-default">Add</button>
                <button class="btn btn-info">Edit selected</button>
                <button class="btn btn-danger">Remove selected</button>

                <div class="table-responsive">

                    <table id="stripes" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>account_id</th>
                            <th>user_local_id</th>
                            <th>country</th>
                            <th>email</th>
                            <th>secret</th>
                            <th>publishable</th>
                            <th>balance api</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>account_id</th>
                            <th>user_local_id</th>
                            <th>country</th>
                            <th>email</th>
                            <th>secret</th>
                            <th>publishable</th>
                            <th>balance api</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($stripes as $stripe)
                            <tr>
                                <td>{{$stripe->account_id}}</td>
                                <td>{{$stripe->user_local_id}}</td>
                                <td>{{$stripe->country}}</td>
                                <td>{{$stripe->email}}</td>
                                <td>{{$stripe->secret}}</td>
                                <td>{{$stripe->publishable}}</td>
                                <td><a target="_blank" href="/stripe/retrieve/balance/{{$stripe->secret}}">view</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>

@endsection

@section("js")
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#stripes').DataTable();
        } );
    </script>
@endsection
