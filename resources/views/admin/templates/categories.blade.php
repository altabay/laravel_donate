@extends('admin.index')

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Categories Table
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <button class="btn btn-default" onclick="addCategory()">Add</button>
                <div class="table-responsive">

                    <table id="categories" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>name</th>
                            <th>slug</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>name</th>
                            <th>slug</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($categories as $category)
                            <tr class="option-{{$category->id}}">
                                <td><input onchange="updateCategory('name', '{{$category->id}}', this)" class="options" value="{{$category->name}}"></td>
                                <td><input onchange="updateCategory('slug', '{{$category->id}}', this)" class="options" value="{{$category->slug}}"></td>
                                <td class="removeOption"><span onclick="removeCategory('{{$category->id}}')" style="cursor: pointer;" class="glyphicon glyphicon-remove"></span></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <?php echo $categories->render(); ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script>
        $(document).ready(function () {
            $('#categories').DataTable();
        });

        function addCategory() {
            $.ajax({
                url: "/admin/add/category",
                type: "post",
                data: {_token: window.Laravel.csrfToken},
                success: function (data) {
                    location.reload();
                }
            });
        }

        function updateCategory(key, optionId, element) {
            $.ajax({
                url: "/admin/update/category",
                type: "post",
                data: {_token: window.Laravel.csrfToken, id: optionId, key: key, value: $(element).val()}
            });
        }

        function removeCategory(optionId) {
            $.ajax({
                url: "/admin/remove/category",
                type: "post",
                data: {_token: window.Laravel.csrfToken, id: optionId},
                success: function (data) {
                    $(".option-"+optionId).remove();
                }
            });
        }
    </script>
@endsection