@extends('admin.index')

@section("css")
@endsection

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Users Table <small>Users configurations</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <p>Documentation</p>

                <div class="left col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">

                    <h1 id="the-checkout-api-calls">The /checkout API calls</h1>

                    <p>The checkout object represents a single payment and defines the amount, the destination account,
                        the application fee, etc. Use the following calls to create, view, and modify checkout objects on WePay:</p>

                    <ul>
                        <li><a href="#lookup">/checkout</a></li>
                        <li><a href="#find">/checkout/find</a></li>
                        <li><a href="#create">/checkout/create</a></li>
                        <li><a href="#cancel">/checkout/cancel</a></li>
                        <li><a href="#refund">/checkout/refund</a></li>
                        <li><a href="#capture">/checkout/capture</a></li>
                        <li><a href="#modify">/checkout/modify</a></li>
                        <li><a href="#release">/checkout/release</a></li>
                    </ul>

                    <h2 id="states">Checkout States</h2>

                    <p>The checkout object has the following states and the following possible state transitions
                        (you can receive callback notifications when the checkout changes state, please read our
                        <a href="https://www.wepay.com/developer/reference/ipn">Instant Payment Notification (IPN) Tutorial</a> for more details):</p>

                    <table class="state-definitions">
                        <tbody>
                        <tr>
                            <td>new</td>
                            <td>The checkout was created by the application. This state only exists for checkouts created in WePay's hosted checkout flow.</td>
                        </tr>
                        <tr>
                            <td>authorized</td>
                            <td>The payer entered their payment info and confirmed the payment on WePay. WePay has successfully charged the card.</td>
                        </tr>
                        <tr>
                            <td>captured</td>
                            <td>The payment has been reserved from the payer.</td>
                        </tr>
                        <tr>
                            <td>released</td>
                            <td>The payment has been credited to the payee account. Note that the released state may be active although there are active partial refunds or partial chargebacks.</td>
                        </tr>
                        <tr>
                            <td>cancelled</td>
                            <td>The payment has been cancelled by the payer, payee, or application.</td>
                        </tr>
                        <tr>
                            <td>refunded</td>
                            <td>The payment was captured and then refunded by the payer, payee, or application. The payment has been debited from the payee account.</td>
                        </tr>
                        <tr>
                            <td>charged back</td>
                            <td>The payment has been charged back by the payer and the payment has been debited from the payee account.</td>
                        </tr>
                        <tr>
                            <td>failed</td>
                            <td>The payment has failed.</td>
                        </tr>
                        <tr>
                            <td>expired</td>
                            <td>Checkouts expire if they remain in the <code>new</code> state for more than 30 minutes (e.g., they have been abandoned). This state only exists for checkouts created in WePay's hosted checkout flow.</td>
                        </tr>
                        </tbody>
                    </table>



                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="notice blue">
                                <h4><p>Tip</p>
                                </h4>
                                <p></p><p>If you do a partial refund for a checkout or there is a partial chargeback from the payer, then the checkout will stay in
                                    state <code class="highlighter-rouge">captured</code> until the whole amount is refunded or charged back. You will however, have the <code class="highlighter-rouge">amount_refunded</code> and <code class="highlighter-rouge">amount_charged_back</code> response parameters to tell you if a partial refund or chargeback have taken place, and if so how much has been refunded or charged back.</p>
                                <p></p>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>

@endsection

@section("js")

@endsection