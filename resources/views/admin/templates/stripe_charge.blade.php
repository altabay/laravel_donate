@extends('admin.index')

@section("css")
@endsection

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Users Table <small>Users configurations</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <button class="btn btn-default">Synchronization</button>

                <div class="table-responsive">

                    <table id="charges" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>checkout_id</th>
                            <th>amount</th>
                            <th>destination</th>
                            <th>status</th>
                            <th>user_id</th>
                            <th>transfer</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>checkout_id</th>
                            <th>amount</th>
                            <th>destination</th>
                            <th>status</th>
                            <th>user_id</th>
                            <th>transfer</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($charges as $charge)
                            <tr>
                                <td>{{$charge->checkout_id}}</td>
                                <td>{{$charge->amount}}</td>
                                <td>{{$charge->destination}}</td>
                                <td>{{$charge->status}}</td>
                                <td>{{$charge->user_id}}</td>
                                <td>{{$charge->transfer}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>

@endsection

@section("js")
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#charges').DataTable();
        } );
    </script>
@endsection
