@extends('admin.index')

@section("css")
@endsection

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Users Table <small>Users configurations</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">

                <button class="btn btn-default">Add</button>
                <button class="btn btn-info">Edit selected</button>
                <button class="btn btn-danger">Remove selected</button>

                <div class="table-responsive">

                    <table id="users" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>user_id</th>
                            <th>user_local_id</th>
                            <th>access_token</th>
                            <th>token_type</th>
                            <th>expires_in</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>user_id</th>
                            <th>user_local_id</th>
                            <th>access_token</th>
                            <th>token_type</th>
                            <th>expires_in</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->user_id}}</td>
                                <td>{{$user->user_local_id}}</td>
                                <td>{{$user->access_token}}</td>
                                <td>{{$user->token_type}}</td>
                                <td>{{$user->expires_in}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("js")
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#users').DataTable();
        } );
    </script>
@endsection