@extends('admin.index')

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Payments Table
                    <small>Payments configurations</small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <button class="btn btn-default">Add</button>
                <button class="btn btn-info">Edit selected</button>
                <button class="btn btn-danger">Remove selected</button>

                <div class="table-responsive">

                    <table id="payments" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>amount</th>
                            <th>status</th>
                            <th>description</th>
                            <th>user_id</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>amount</th>
                            <th>status</th>
                            <th>description</th>
                            <th>user_id</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($payments as $payment)
                            <tr>
                                <td>{{$payment->amount}}</td>
                                <td>{{$payment->status}}</td>
                                <td>{{$payment->description}}</td>
                                <td>{{$payment->user_id}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <?php echo $payments->render(); ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script>
        $(document).ready(function() {
            $('#payments').DataTable();
        } );
    </script>
@endsection