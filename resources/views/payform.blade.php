@extends('layouts.app')
@section('content')
    <style>
        /**
        * The CSS shown here will not be introduced in the Quickstart guide, but shows
        * how you can use CSS to style your Element's container.
        */
        .StripeElement {
            background-color: white;
            padding: 8px 12px;
            border-radius: 4px;
            border: 1px solid transparent;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
    </style>

    <div class="container">
        <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
                <h2>DONATE NOW FOR {{$company->name}}</h2>

                @if(!isset($methodPay))
                <form class="form-inline" method="post" action="/paymethod">
                    <input type="radio" checked name="methodPay" value="wepay"> WePay<br>
                    <input type="radio" name="methodPay" value="stripe"> Strip<br>
                    <input type="hidden" name="company_id" value="{{$company->id}}">
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                        <div class="input-group">
                            {!! csrf_field() !!}
                            <div class="input-group-addon">$</div>
                            <input type="text" required class="form-control" id="exampleInputAmount" placeholder="Amount" name="amount">
                            <div class="input-group-addon">.00</div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Transfer cash</button>
                </form>
                @endif

                @if(isset($methodPay))
                    @if($methodPay == "wepay")
                        <div id="wepay_checkout"></div>
                        <script type="text/javascript" src="https://www.wepay.com/min/js/iframe.wepay.js">
                        </script>
                        <script type="text/javascript">
                            WePay.iframe_checkout("wepay_checkout", "{{$checkoutUri}}");
                        </script>
                    @endif
                        @if($methodPay == "stripe")
                            {{--<form action="/stripe/charge" method="POST">
                                {!! csrf_field() !!}
                                <script
                                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                        data-key="pk_test_eEiW9mXcLEd1RmjAX0iwshHO"
                                        data-amount="{{$amount}}"
                                        data-name="Demo Site"
                                        data-description="Widget"
                                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                        data-locale="auto">
                                </script>
                            </form>--}}
                    @endif
                @endif
            </div>
            <div class="col-lg-4">

            </div>
        </div>
    </div>
@endsection

@section("js")

@endsection


