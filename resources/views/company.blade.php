@extends('layouts.app')

<style>
    #app .container {
        width: 100%;
    }
</style>
<link href="{{asset('css/social-buttons.css')}}" rel="stylesheet">

<?php
$companyImagesArray = json_decode($company->images);
if (!isset($companyImagesArray[0])) {
    $companyImagesArray[0] = "";
}
?>
@section("header")
    <meta property="og:image" content="/thumbnail/{{$companyImagesArray[0]}}" />
    <meta property="og:description" content="{{$company->description}}" />
    <meta property="og:url"content="<?php echo url()->current(); ?>" />
    <meta property="og:title" content="{{$company->name}}" />
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <h3>My Companies !</h3>

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">

                                    <img width="300" src="/thumbnail/{{$companyImagesArray[0]}}" alt="Avatar">

                                    <p>name: {{$company->name}}</p>
                                    <p>description: {{$company->description}}</p>
                                    <p>amount_need: {{$company->amount_need}}</p>
                                    <p>amount_now: {{$company->amount_now}}</p>
                                    <div id="shareBtn" class="btn btn-success clearfix">Share</div>
                                    <p></p>
                                    <a href="https://twitter.com/intent/tweet?original_referer=http://google.omc&text=<?php echo url()->current(); ?> <---> text test here&tw_p=tweetbutton&url"
                                       class="btn btn-twitter twitter-share-button">Twitter</a>

                                    <p></p>
                                    <a class="btn btn-success" href="/donete/for/{{$company->slug}}">Donate Now</a>
                                </div>
                                <div class="col-md-6">
                                    <hr>
                                    <h4> Leave a Comment</h4>
                                    <form id="post_comment" action="/add/comment/to/company" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" value="{{$company->id}}" name="id">
                                        <textarea name="text" class="form-control" rows="3"></textarea>
                                        <p></p>
                                        <button type="submit" style="text-align: center" class="btn">Post Comment
                                        </button>
                                    </form>
                                    <hr>
                                    @foreach($comments as $comment)
                                        <p>{{$comment->text}}</p>
                                    @endforeach
                                </div>
                                <div class="col-md-6">
                                    <hr>
                                    <h4> Recent Donations </h4>
                                    @foreach($donates as $donate)
                                        <p>@if($donate->anonymous != 1) {{$donate->first_name}} @else
                                                Anonymous @endif{{$donate->amount}}$: {{$donate->description}}</p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script>
        jQuery('.twitter-share-button').click(function (e) {
            e.preventDefault();
            var width = 500;
            var height = 300;
            window.open(this.href, 'newwindow', 'width=' + width + ', height=' + height + ', top=' + ((window.innerHeight - height) / 2) + ', left=' + ((window.innerWidth - width) / 2));
        });
    </script>

    <?php $res = ($_SERVER['SERVER_NAME'] == "trader.cf")? '1356061454484077': '1356054664484756'; ?>

    <script>
        document.getElementById('shareBtn').onclick = function () {
            FB.ui({
                method: 'share',
                display: 'popup',
                href: '<?php echo url()->current(); ?>',
                app_id: '<?php echo $res?>'
            }, function (response) {
            });
        }
    </script>

    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.9&appId=<?php echo $res?>";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection



