@extends('layouts.app')

<style>
    #app .container {
        width: 100%;
    }
</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <h3>Company Form</h3>

                        @if( \Illuminate\Support\Facades\Session::has( 'message' ))
                            <div class="color-swatch brand-primary" style="background: #3cc3ff">{{ \Illuminate\Support\Facades\Session::get( 'message' ) }}</div>
                        @endif

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="bs-example" data-example-id="simple-table">

                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <form id="create_company" action=" @if(isset($company)) /update/company @else /create/new/company @endif" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            @if(isset($company))
                                                <input type="hidden" value="{{$company->id}}" name="id">
                                            @endif
                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-2 col-form-label">Name</label>
                                                <div class="col-10">
                                                    <input class="form-control" type="text"id="name" name="name" value="@if(isset($company)){{$company->name}}@endif">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-2 col-form-label">Description</label>
                                                <div class="col-10">
                                                    <textarea class="form-control" id="description" name="description" rows="5" >@if(isset($company)){{$company->description}}@endif</textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-2 col-form-label">Amount Need</label>
                                                <div class="col-10">
                                                    <input class="form-control" type="text"  id="amount_need" name="amount_need" value="@if(isset($company)){{$company->amount_need}}@endif">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-2 col-form-label">Category</label>
                                                <div class="col-10">
                                                    <select id="category_id" name="category_id" class="form-control">
                                                        @foreach($categories as $category)
                                                            <option @if(isset($company) && $company->category_id == $category->id) selected @endif  value="{{$category->id}}">{{$category->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-2 col-form-label">ZIP or Postal Code</label>
                                                <div class="col-10">
                                                    <input class="form-control" type="text"  id="zip_postal" name="zip_postal" value="@if(isset($company)){{$company->zip_postal}}@endif">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-2 col-form-label">Visitor Comments</label>
                                                <div class="col-10">
                                                    <input type="checkbox" @if(isset($company) && $company->visitor_comments) checked="checked" @endif id="visitor_comments" name="visitor_comments" aria-label="...">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-2 col-form-label">Donations Enabled</label>
                                                <div class="col-10">
                                                    <input type="checkbox" @if(isset($company) && $company->donations_enabled) checked="checked" @endif id="donations_enabled" name="donations_enabled" aria-label="...">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-2 col-form-label">Daily Facebook Post</label>
                                                <div class="col-10">
                                                    <input type="checkbox" @if(isset($company) && $company->auto_fb_post_mode) checked="checked" @endif id="auto_fb_post_mode" name="auto_fb_post_mode" aria-label="...">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-2 col-form-label">Display on Site?</label>
                                                <div class="col-10">
                                                    <input type="checkbox" @if(isset($company) && $company->visible) checked="checked" @endif id="visible" name="visible" aria-label="...">
                                                </div>
                                            </div>

                                            <br>
                                            <label class="control-label">Photos</label>
                                            <input id="input-24" name="images[]" type="file" multiple class="file-loading">
                                            <br>
                                            <button class="btn btn-primary">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("js")

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="/bootstrap-fileinput-master/js/plugins/canvas-to-blob.js" type="text/javascript"></script>
    <script src="/bootstrap-fileinput-master/js/plugins/sortable.js" type="text/javascript"></script>
    <script src="/bootstrap-fileinput-master/js/plugins/purify.js" type="text/javascript"></script>
    <script src="/bootstrap-fileinput-master/js/fileinput.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/bootstrap-fileinput-master/js/locales/uk.js"></script>

   <script>
       $(document).on('ready', function() {

           $("#input-24").fileinput({

               @if(isset($company))
               <?php $companyImagesArray = json_decode($company->images);?>
               @if(isset($companyImagesArray))
               initialPreview: [
                   @foreach($companyImagesArray as $image)
                   <?php echo "'/thumbnail/$image'," ?>
                   @endforeach
               ],
               initialPreviewAsData: true,
               initialPreviewConfig: [
                   @foreach($companyImagesArray as $key => $image)
                   <?php echo "{caption: \"$image\", width: \"120px\", key: \"$image\"},";?>
                   @endforeach
               ],
               @endif
               @endif

               /*initialPreview: [
                   'http://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/FullMoon2010.jpg/631px-FullMoon2010.jpg',
                   'http://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Earth_Eastern_Hemisphere.jpg/600px-Earth_Eastern_Hemisphere.jpg'
               ],
               initialPreviewAsData: true,
               initialPreviewConfig: [
                   {caption: "Moon.jpg", size: 930321, width: "120px", key: 1},
                   {caption: "Earth.jpg", size: 1218822, width: "120px", key: 2}
               ],*/

               @if(isset($company))
                    deleteUrl: "/remove/company/image?_token="+window.Laravel.csrfToken+"&company_id={{$company->id}}",
               @else
                    deleteUrl: "/remove/company/image?_token="+window.Laravel.csrfToken+"",
               @endif

               overwriteInitial: false,
               maxFileSize: 1000,
               initialCaption: "Select Photos"
           });
       });
   </script>
@endsection

<div id="fb-root"></div>

