<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 20; $i++){
            DB::table('companies')->insert([
                'name' => str_random(10),
                'description' => str_random(100),
                'slug' => str_random(10),
                'status' => "active",
                'amount_need' => rand(100,2000),
                'amount_now' => rand(50,200),
                'user_id' => rand(1,19),
                'type' => "simple",
            ]);
        }

        DB::table('companies')->insert([
            'name' => str_random(10),
            'description' => str_random(100),
            'slug' => str_random(10),
            'status' => "active",
            'amount_need' => rand(100,2000),
            'amount_now' => rand(50,200),
            'user_id' => 19,
            'type' => "simple",
        ]);

        DB::table('companies')->insert([
            'name' => str_random(10),
            'description' => str_random(100),
            'slug' => str_random(10),
            'status' => "active",
            'amount_need' => rand(100,2000),
            'amount_now' => rand(50,200),
            'user_id' => 19,
            'type' => "simple",
        ]);
    }
}
