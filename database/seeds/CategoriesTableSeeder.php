<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ["Medical", "Volunteer", "Education", "Sport"];
        for ($i = 0; $i < 4; $i++){
            DB::table('categories')->insert([
                'name' => $array[$i],
                'slug' => str_random(10)
            ]);
        }
    }
}
