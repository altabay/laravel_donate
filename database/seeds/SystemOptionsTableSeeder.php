<?php

use Illuminate\Database\Seeder;

class SystemOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('system_options')->insert([
            'key' => 'account_id',
            'value' => '850820223',
        ]);

        DB::table('system_options')->insert([
            'key' => 'client_id',
            'value' => '13188',
        ]);

        DB::table('system_options')->insert([
            'key' => 'client_secret',
            'value' => '023c60b20f',
        ]);

        DB::table('system_options')->insert([
            'key' => 'access_token',
            'value' => 'STAGE_b1f9b5e9460d13cd0912d9271cdef6f355769cf61ec5c1da78e0b07b37de1b06',
        ]);
    }

}
