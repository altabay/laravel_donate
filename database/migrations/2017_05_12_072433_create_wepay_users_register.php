<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWepayUsersRegister extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wepay_users_register', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->integer('user_local_id')->unsigned()->nullable();
            $table->foreign('user_local_id')->references('id')->on('users')->onDelete('set null');
            $table->string('access_token');
            $table->string('token_type');
            $table->string('email')->nullable();
            $table->integer('expires_in');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wepay_users_register');
    }
}
